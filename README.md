# concrete58_happyBirthday
[![concrete58_happyBirthday](//radeff.red/pics/git/happybirthday.png)](concrete58HappyBirthday)

## Français
un block concrete5 pour générer envoyer des emails de "bon anniversaire" automatiquement le "jour J"

# Instructions pour l'installation
- Installer le package sous votre répertoire /packages
- Chercher dans votre interface d'administration "Améliorer concrete5"
- Chercher le packages _"Concrete58HappyBirthday"_
- Installer le package

Vous pouvez maintenant:
- inclure le bloc _Chronos_ où vous le souhaitez sur votre site
- administrer les données dans le _dashboard_ avec l'entrée "Chronologies"

---

## English:
a block to provides historical events happened in the past the same day as the present day

# Installation Instructions
- Install package in your site's /packages directory
- Go to "Extend concrete5 > Add Functionality"
- Activate package _"Concrete58HappyBirthday"_

You may now:
- include the block "HappyBirthday" anywhere on your concrete5 website
- administer the SQL table with the dashboard entry "HappyBirthday"
