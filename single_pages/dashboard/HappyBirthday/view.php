 <?php defined('C5_EXECUTE') or die(_("Access Denied."));

 //get current c5 path
$zepath= DIR_REL ."/dashboard/";

 function chercher($cherche) {
   $db = Loader::db();
   $blocks = array();
   $aujourdhui=date("m-d");
   $sql="SELECT * FROM happybirthday WHERE date LIKE '%".$aujourdhui."' ORDER BY Rand() LIMIT 0,1";
   $sql="SELECT * FROM happybirthday ORDER BY id DESC LIMIT 0,10";
   $sql="SELECT * FROM happybirthday WHERE lib LIKE '%".$cherche."%' ORDER BY id DESC";
   //echo $sql; exit;
   $blocks = $db->Execute($sql);
   return $blocks;
 }

 function voir($id) {
   global $blocks;
   //$myString = t('<blink>Hello</blink>');
   //return $myString;
   //Database::setDebug(true);
   $db = Loader::db();
   $blocks = array();
   $aujourdhui=date("m-d");
   $sql="SELECT * FROM happybirthday WHERE id=".$id;
   //echo "<pre>$sql</pre>";
   $blocks = $db->Execute($sql);
   return $blocks;
 }

function supprime($id) {
   //$myString = t('<blink>Hello</blink>');
   //return $myString;
   //Database::setDebug(true);
   $db = Loader::db();
   $blocks = array();
   $sql="DELETE FROM happybirthday WHERE id=".$id;
   $blocks = $db->Execute($sql);
 }

 function insere($date,$lib) {
    //$myString = t('<blink>Hello</blink>');
    //return $myString;
    //Database::setDebug(true);
    $db = Loader::db();
    $blocks = array();
    $sql="INSERT INTO `happybirthday` (`id`, `date`, `lib`, `pays`) VALUES (NULL, '" .$date ."', '" .htmlentities($lib) ."', '');";
    $blocks = $db->Execute($sql);
    return $blocks;
  }

  function modifie($id,$date,$lib) {
      $db = Loader::db();
      $blocks = array();
      //$lib= addslashes(htmlentities($lib));
      $lib= addslashes($lib);
      $sql="UPDATE `happybirthday` SET `date` = '".$date."', `lib` ='".$lib."' WHERE `id` = ".$id;
      $blocks = $db->Execute($sql);

    }
 ?>
<h1><a href="happybirthday">Chronologies</a>&nbsp;<a href="happybirthday?action=new" class="fa fa-plus-square"></a></h1>

<div class="happybirthday">
<?php
//new entry
if($_GET['action']=="new"){
  echo "<h2>Nouvelle chronologie</h2>";
  echo '<form method="get" action="happybirthday?action=insert">';
  echo '<input type="hidden" name="action" value="insert">';
  echo '<div class="ccm-dashboard-express-form">
        <fieldset>
        <div class="form-group">
        <label class="control-label">date (YYYY-MM-DD)</label>
          <span class="text-muted small">Obligatoire</span>
          <input type="text" name="date" class="form-control"/>
        </div>
        <div class="form-group">
        <label class="control-label">Libellé</label>
            <span class="text-muted small">Obligatoire</span>
            <textarea name="lib" class="form-control" style="height: 300px"></textarea>
        </div>
        </fieldset>
        </div>
       <div class="ccm-dashboard-form-actions-wrapper">
            <div class="ccm-dashboard-form-actions">
              <a class="pull-left btn btn-default" href="/happybirthday">Retour</a>
              <button class="pull-right btn btn-primary" type="submit">Enregistrer</button>
            </div>
        </div>
    ';


//insert new entry
} else if($_GET['action']=="insert"){
  echo "<h2>insertion nouvelle chronologie</h2>";
  $date=$_GET['date'];
  $lib=htmlentities($_GET['lib']);
  $sql="INSERT INTO `happybirthday` (`id`, `date`, `lib`) VALUES (NULL, '" .$date ."', '" .$lib ."');";
  insere($date,$lib);
    echo "<h2>Chronologie insérée &nbsp;<a class=\"fa fa-home\" href=\"happybirthday\"></a></h2>";

//delete entry
} else if($_GET['action']=="delete"){
  supprime($_GET['id']);
  echo "<h2>Chronologie #" .$_GET['id']." supprimée&nbsp;<a class=\"fa fa-home\" href=\"happybirthday\"></a></h2>";

//edit entry
} else if($_GET['action']=="edit"){
  global $blocks;
  echo "<h2>Modifier chronologie #id" .$_GET['id']."</h2>";
  $id=$_GET['id'];
  voir($id);
  //var_dump($blocks);
  foreach ($blocks as $anniversaire):
    $id= $anniversaire['id'];
    $lib= $anniversaire['lib'];
    $date= $anniversaire['date'];
  endforeach;


  echo '<form method="get" action="happybirthday">';
  echo '<input type="hidden" name="action" value="modifie2">';
  echo '<input type="hidden" name="id" value="'.$id.'">';
  echo '<div class="ccm-dashboard-express-form">
        <fieldset>
        <div class="form-group">
        <label class="control-label">date (YYYY-MM-DD)</label>
          <span class="text-muted small">Obligatoire</span>
          <input type="text" name="date" class="form-control" value="'.$date.'"/>
        </div>
        <div class="form-group">
        <label class="control-label">Libellé</label>
            <span class="text-muted small">Obligatoire</span>
            <textarea name="lib" class="form-control"  style="height: 300px">'.$lib.'</textarea>
        </div>
        </fieldset>
        </div>
           <div class="ccm-dashboard-form-actions-wrapper">
                <div class="ccm-dashboard-form-actions">
                  <a class="pull-left btn btn-default" href="'.$zepath.'happybirthday">Retour</a>
                  <button class="pull-right btn btn-primary" type="submit">Enregistrer</button>
                </div>
            </div>
    ';

  } else if($_GET['action']=="modifie2"){
    echo "<h2>modifier chronologie</h2>";
    $id=$_GET['id'];
    $date=$_GET['date'];
    $lib=htmlentities($_GET['lib']);
modifie($id,$date,$lib);
      echo "<h2>Chronologie modifiée &nbsp;<a class=\"fa fa-home\" href=\"happybirthday\"></a></h2>";


//no action display entry list
} else {
//getServiceIconHTML();
echo '<form action="happybirthday" method="get">
  <input type="text" name="chercher">
  <input type="submit">
</form>';

$blocks=chercher($_GET['chercher']);
echo "<table class=\"table table-striped table-bordered table-hover dataTable no-footer\">";
echo "<tbody>";
foreach ($blocks as $anniversaire):
//var_dump($anniversaire);
$date=$anniversaire['date'];
$an=substr($date,0,4);
$mois=substr($date,5,2);
$jour=substr($date,8,2);
echo "<tr class=\"happybirthday\">";
echo "<td>";
echo $jour;
echo "</td><td>";
echo $mois;
echo "</td><td>";
echo $an;
echo "</td><td>";
echo html_entity_decode($anniversaire['lib']);
echo "</td>";
echo "</td><td>";
echo '<a class="fa fa-edit" href="happybirthday?action=edit&id='.$anniversaire['id'].'"></a>';
echo "&nbsp;";
echo '<a class="fa fa-trash" href="happybirthday?action=delete&id='.$anniversaire['id'].'"></a>';

echo "</td>";

echo "</tr>";
endforeach;
echo "</tbody>";
echo "</table>";
}
 ?>
