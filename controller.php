<?php
/**
 *
 * The controller for the Concrete58Happybirthday package.
 *
 * @package    Chronos
 *
 * @author     Fred Radeff <fradeff@akademia.ch>
 * @copyright  Copyright (c) 2020 radeff.red (https://radeff.red)
 * @license    http://www.gnu.org/licenses/ GNU General Public License
 */
namespace Concrete\Package\Concrete58Happybirthday;
use Package;
use BlockType;
use View;
use Loader;
use SinglePage;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package {

	protected $pkgHandle = 'concrete58_happybirthday';
	protected $appVersionRequired = '5.8.5.4';
	protected $pkgVersion = '0.0.1';


	public function getPackageName()
	{
		return t("Concrete58Happybirthday");
	}

	public function getPackageDescription()
	{
		return t("Generate automatic e-mails for birthdays");
	}

	public function install()
	{
		$pkg = parent::install();
		// install block
		BlockType::installBlockTypeFromPackage('happybirthday', $pkg);
		// Install Single Pages
		SinglePage::add('/dashboard/happybirthday',$pkg);

				$this->seedData($pkg, 'happybirthday.sql');

		return $pkg;
	}
/*** UTILITY FUNCTIONS ***/
	private function seedData($pkg, $filename) {
		//NOTE that you can only run one query at a time,
		// so each sql statement must be in its own file!
		$db = Loader::db();
		$sql = file_get_contents($pkg->getPackagePath() . '/seed_data/' . $filename);
		$r = $db->execute($sql);
		if (!$r) {
			throw new Exception(t('Unable to install data: %s', $db->ErrorMsg()));
		}
	}

	public function uninstall() {
		parent::uninstall();
		$db = \Database::connection();
		$db->query('drop table happybirthday');
		$db->query('drop table happybirthdayMessage');

	}

}

/*
db.xml


https://documentation.concrete5.org/developers/packages/custom-database-tables-in-packages/db-xml-doctrine-xml-format
https://documentation.concrete5.org/developers/database-management/accessing-database-make-queries
https://documentation.concrete5.org/developers/packages/concrete5-cif-format

db.xml Field Type to MySQL
db.xml	MySQL
C	VARCHAR
XL	LONGTEXT
X	TEXT
C2	VARCHAR
X2	LONGTEXT
B	LONGBLOB
D	DATE
TS	DATETIME
T	DATETIME
I4	INTEGER
I	INTEGER
I1	TINYINT
I2	SMALLINT
I8	BIGINT
F	DOUBLE
N	NUMERIC
 */
